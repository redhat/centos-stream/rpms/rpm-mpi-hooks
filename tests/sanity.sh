#!/bin/bash
#
# This is a simple sanity test to satisfy the RHEL8.1 onboard gating
# requirement.

ret=1

if [ -e /usr/lib/rpm/mpi.prov ] && [ -e /usr/lib/rpm/mpi.req ]; then
  ret=0
fi

exit $ret
